# Salt and Sanctuary Cheat Table

Game version - 1.0.0.6

Cheat Engine - 7.0+

1. Download
2. Open in CE
3. Enjoy!!!

## Features
* Character params edit
* Character params freeze -> Num0 for HP/Stamina, Num. for Torch
* Character flying mode -> NumLeft/NumRight/NumUp/NumDown
* Inventory selected item editor -> highlight item then activate record
* Advanced Inventory item editor -> open inventory page with items then activate script
* Inventory Viewer, editable
* Teleport to Sanctuaries, Bosses and key NPC
* Pickup Edit for dropped item (in research)
* Static viewer for NPC params
* Static viewer for NPC currently spawned
* Sanctuaries Management Tool
* Items DB / ingame Items Types Lists
* Misc Items Types Lists

